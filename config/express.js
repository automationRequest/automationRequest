var config = require('./config'),
	express = require('express'),
	bodyParser = require('body-parser'),
	fs = require('fs'),
	csv = require("fast-csv"),
	path = require('path');
module.exports = function() {
	var app = express();
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	app.use(bodyParser.json());
	
	app.set('views', './app/views');
	app.set('view engine', 'ejs');

	// app.use(flash());
	// app.use(passport.initialize());
	// app.use(passport.session());
	require('../app/routes/index.server.routes.js')(app);
	require('../app/routes/employee.server.routes.js')(app);
	require('../app/routes/applicationModule.server.routes.js')(app);
	require('../app/routes/approver.server.routes.js')(app);
	require('../app/routes/request.server.routes.js')(app);
	require('../app/routes/dashboard.server.routes.js')(app);
	
	app.use(express.static('./public'));
	// app.use(express.static(path.join(__dirname, 'public')));
	return app;
};