var mongoose = require('mongoose'),
Schema = mongoose.Schema;


var UsergroupSchema = new Schema({
	name:  { type: String, required: true, unique: true }
	,qualifiers: [{ type: Schema.Types.ObjectId, ref: 'Qualifier' }]
	,approvers : [{ type: Schema.Types.ObjectId, ref: 'Approver' }]
	,created_at: Date
	,updated_at: Date
});


UsergroupSchema.pre('save', function(next) {
	var currentDate = new Date();
	this.updated_at = currentDate;
	if (!this.created_at)
		this.created_at = currentDate;
	next();
});

mongoose.model('Usergroup', UsergroupSchema);