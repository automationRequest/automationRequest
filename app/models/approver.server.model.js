var mongoose = require('mongoose'),
Schema = mongoose.Schema;


var ApproverSchema = new Schema({
	gsid : Number
	,name:  String
	,domainId : String
	,email : String 
	,created_at: Date
	,updated_at: Date
});


ApproverSchema.pre('save', function(next) {
	var currentDate = new Date();
	this.updated_at = currentDate;
	if (!this.created_at)
		this.created_at = currentDate;
	next();
});

mongoose.model('Approver', ApproverSchema);