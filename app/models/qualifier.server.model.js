var mongoose = require('mongoose'),
Schema = mongoose.Schema;


var QualifierSchema = new Schema({
	name:  { type: String, required: true, unique: true }
	,approvers : [{ type: Schema.Types.ObjectId, ref: 'Approver' }]
	,created_at: Date
	,updated_at: Date
});


QualifierSchema.pre('save', function(next) {
	var currentDate = new Date();
	this.updated_at = currentDate;
	if (!this.created_at)
		this.created_at = currentDate;
	next();
});

mongoose.model('Qualifier', QualifierSchema);