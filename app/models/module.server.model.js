var mongoose = require('mongoose'),
Schema = mongoose.Schema;


var ModuleSchema = new Schema({
	name:  { type: String, required: true, unique: true}
	,usergroups : [{ type: Schema.Types.ObjectId, ref: 'Usergroup' }]
	,created_at: Date
	,updated_at: Date
});

ModuleSchema.pre('save', function(next) {
	var currentDate = new Date();
	this.updated_at = currentDate;
	if (!this.created_at)
		this.created_at = currentDate;
	next();
});

mongoose.model('Module', ModuleSchema);