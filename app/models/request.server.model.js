var mongoose = require('mongoose'),
Application = mongoose.model('Application').schema,
Usergroup = mongoose.model('Usergroup').schema,
Module = mongoose.model('Module').schema,
Approver = mongoose.model('Approver').schema,
Qualifier = mongoose.model('Qualifier').schema,
Qualifiervalue = mongoose.model('Qualifiervalue').schema
Schema = mongoose.Schema;


var RequestSchema = new Schema({
	sr_no:  { type: String, required: true, unique: true}
	,user_id : Number
	,application : { type: Schema.Types.ObjectId, ref: 'Application'}
	,usergroup : { type: Schema.Types.ObjectId, ref: 'Usergroup'}
	,module : { type: Schema.Types.ObjectId, ref: 'Module'}
	,approver : [{ type: Schema.Types.ObjectId, ref: 'Approver'}]
	,qualifier : { type : Schema.Types.ObjectId, ref : 'Qualifier'}
	,qualifiervalue : { type : Schema.Types.ObjectId, ref : 'Qualifiervalue'}
	,request_date : Date
	,status : String
	,remarks : String
	,created_at: Date
	,updated_at: Date
});

RequestSchema.pre('save', function(next) {
	var currentDate = new Date();
	this.updated_at = currentDate;
	if (!this.created_at){
		this.created_at = currentDate;
		this.status = 'In Progress';
	}
	next();
});

mongoose.model('Request', RequestSchema);