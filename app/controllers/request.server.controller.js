var mongoose = require('mongoose'),
	Request = mongoose.model('Request'),
 	ObjectId = require('mongoose').Types.ObjectId; 

var http = require('http');
var url = require('url') ;


var getErrorMessage = function(err) {
	var message = '';
	for (var errName in err.errors) {
		if (err.errors[errName].message)
			message = err.errors[errName].message;
	}
	return message;
};

exports.render = function(req, res) {
	res.render('request/index', {
		title: 'Automation Request Pool '
	});
};

exports.createView = function(req, res){
	res.render('request/form', {
		title: 'Create Request'
	});
};

exports.createDashboardView = function(req, res){
	res.render('request/dashboard', {
		title: 'Dashboard'
	});
};

exports.list = function(req, res, next) {
	Request.find({}, function(err, applications) {
		if (err) {
			return next(err);
		}
		else {
			res.json(applications);
		}
	}).populate('application usergroup module qualifier approver');
};

exports.create = function(req, res, next) {
	var request = new Request(req.body);
	request.save(function(err) {
		if (err) {
			return next(err);
		}
		else {
			res.json(request);
		}
	});
};

exports.update = function(req, res, next) {
	Request.findByIdAndUpdate(req.request.id, req.body, function(err, request) {
		if (err) {
			return next(err);
		}
		else {
			res.json(request);
		}
	});
};


exports.read = function(req, res) {
	res.json(req.request);
};

exports.getById = function(req, res, next, id) {
	Request.findOne({
		_id: id
	},
	function(err, request) {
		if (err) {
			return next(err);
		}
		else {
			req.request = request;
			next();
		}
	}
	);
};

exports.delete = function(req, res, next) {
	req.application.remove(function(err) {
		if (err) {
			return next(err);
		}
		else {
			res.json(req.application);
		}
	})
};

exports.searchBy = function(req, res, next){
	if(req.body.approver){
		req.body.approver = { $elemMatch:{$eq: new ObjectId(req.body.approverId)}};
		delete req.body.approverId;
	}
	if(req.body.request_date){
		var request_date =  req.body.request_date;
		var tomorrow = new Date(request_date);
		tomorrow.setDate(tomorrow.getDate() + 2);
		req.body.request_date = {"$gte": new Date(request_date), "$lt": tomorrow};
	}
	if(Object.keys(req.body).length !== 0){
		Request.find(req.body, function(err, applications) {
				if (err) {
					console.log(err);
					return next(err);
				}
				else {
					res.json(applications);
				}
		}).populate('application usergroup module qualifier qualifiervalue approver');
	}else{
		res.send('');
	}
};

exports.batchSend = function(req,res, next){
	var hostname = req.headers.host; 
	var pathname = url.parse(req.url).pathname; 

	Request.find({ sr_no : { $in : req.body.srNos }}, function(err, requests){
		if(err){
			console.log(err);
			return next(err);
		}else{
			requests.forEach(function(request, i){
				var approvers = [];
				request.approver.forEach(function(approver){
					approvers.push(approver.email);
				});
				var result = sendEmail(approvers.join(';'), 'http://' + hostname  + '/request/approvrView?srNo='+req.body.srNos);
				if(result = 1){
					res.json('Successful');
				}
			});
		}
	}).populate('approver');
}

exports.getRequestBySrNo = function(req, res) {
	Request.findOne({
		sr_no: req.body.sr_no
	},
	function(err, request) {
		if (err) {
			return next(err);
		}
		else {
			res.json(request);
		}
	}
	).populate('application usergroup module qualifier approver');
};

exports.approvrView = function(req,res){
	res.render('request/approver',{
		title : 'Automation Request Tool'
	});
}

exports.updateSrStatus = function(req, res){
	var status = req.body.status;
	req.request.status = status;
	req.request.save(function(){
		res.json(req.request)
	});
	
}

function sendEmail(emails, url){
	var nodemailer = require('nodemailer');
	var smtpTransport = require('nodemailer-smtp-transport');
	var options = {
	    host: 'smtpapp1.oocl.com'
	};
	
	var transporter = nodemailer.createTransport(smtpTransport(options)) 
	var mailOptions = {
	    from: 'EASC-NO-REPLY@oocl.com', // sender address 
	    to: emails, // list of receivers 
	    subject: 'Access Request for approval',
	    html: '<span> Dear Approver(s), <br/><br/>\
	    	   Please click the <a href='+url+'>link</a> to review the access request and update its status. Thanks.<br/><br/>\
	    	   Kind Regards, <br/>\
	    	   SYSADMIN</span>'
	};

	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        return console.log(error);
	    }
	    console.log('Message sent: ' + info.response);
	    return 1;
	});
}