var Application = require('mongoose').model('Application');

var getErrorMessage = function(err) {
	var message = '';
	for (var errName in err.errors) {
		if (err.errors[errName].message)
			message = err.errors[errName].message;
	}
	return message;
};

exports.list = function(req, res, next) {
	Application.find({}, function(err, applications) {
		if (err) {
			return next(err);
		}
		else {
			res.json(applications);
		}
	});
};

exports.create = function(req, res, next) {
	var application = new Application(req.body);
	application.save(function(err) {
		if (err) {
			return next(err);
		}
		else {
			res.json(application);
		}
	});
};

exports.read = function(req, res) {
	res.json(req.application);
};

exports.getById = function(req, res, next, id) {
	Application.findOne({
		_id: id
	},
	function(err, application) {
		if (err) {
			if(id == 0){
				next();
			}else{
				return next(err);
			}
		}
		else {
			req.application = application;
			next();
		}
	}
	).populate('modules');
};

exports.update = function(req, res, next) {
	User.findByIdAndUpdate(req.application.id, req.body, function(err, application) {
		if (err) {
			return next(err);
		}
		else {
			res.json(application);
		}
	});
};

exports.delete = function(req, res, next) {
	req.application.remove(function(err) {
		if (err) {
			return next(err);
		}
		else {
			res.json(req.application);
		}
	})
};