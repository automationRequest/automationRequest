var Application = require('mongoose').model('Application'),
Module = require('mongoose').model('Module'),
Usergroup = require('mongoose').model('Usergroup'),
Qualifier = require('mongoose').model('Qualifier'),
Approver = require('mongoose').model('Approver'),
Qualifiervalue = require('mongoose').model('Qualifiervalue'),
async = require('async');

exports.render = function(req, res) {
	res.render('application/index', {
		title: 'Maintain Application Module'
	});
};

exports.save = function(req, res){
	var val = composeDocument(req);
	if(val > 0){
		res.send('error while saving');
	}else{
		res.send('successfully saved');
	}
	// console.log(req.module);
	// req.application.modules.push(req.module);
	// req.application.save();
	// console.log(req.application);
};

function composeDocument(req){
	var res = 0;
	if(req.qualifier == null && req.approver != null){
		res += addToDocument(req.usergroup, req.approver, 'approvers');
	}
	res += addToDocument(req.qualifier, req.approver, 'approvers');
	res += addToDocument(req.usergroup, req.qualifier, 'qualifiers');
	res += addToDocument(req.module, req.usergroup, 'usergroups');
	res += addToDocument(req.application, req.module, 'modules');
	return res;
}

function addToDocument(parent, child, key){
	if(parent != null && child != null && !exists(parent[key], child)){
		parent[key].push(child);
		parent.save(function(err){
			if(err){
				return 1;
			}
		});
	}
}

function exists(arrObject, obj){
	var res = false;
	for (var i = arrObject.length - 1; i >= 0; i--) {
		if(obj._id.equals(arrObject[i]._id)){
			res = true;
			break;
		}
	};
	return res;
}

exports.list = function(req, res){
	Application.find({}, function(err, applications) {
		if (err) {
			return next(err);
		}
		else {
			async.series([
				function(next){
					async.each(applications, function(application, cbAp){
						var modCounter = 0;
						async.each(application.modules, function(moduleId, cbMod){
							Module.findOne({ _id: moduleId},function(err, module) {
								var ugCounter = 0;
								async.each(module.usergroups, function(usegroupId, cbUg){
									Usergroup.findOne({ _id : usegroupId}, function(err, usergroup){
										var qualCounter = 0;
										async.each(usergroup.qualifiers, function(qualifierId, cbQual){
											Qualifier.findOne({_id : qualifierId}, function(err, qualifier){
												usergroup.qualifiers[qualCounter] = qualifier;
												qualCounter++;
												cbQual();
											});
										}, function(err){
											module.usergroups[ugCounter] = usergroup;
											ugCounter++;
											cbUg();
										});
									}); 
								}, function(err){
									application.modules[modCounter] = module;
									modCounter++;
									cbMod();
								});
							});
						}, function(err){
							cbAp();
						});
					}, function(err){
						next();
					});
				}
			], function(err, result){
				res.json(applications);
			});
		}
});
};

exports.delete = function(req, res){
	req.application.modules.pull(req.module);
	req.application.save(function(err, application){
		if(!err){
			res.json(application);
		}
	});
};