var Module = require('mongoose').model('Module');

var getErrorMessage = function(err) {
	var message = '';
	for (var errName in err.errors) {
		if (err.errors[errName].message)
			message = err.errors[errName].message;
	}
	return message;
};

exports.list = function(req, res, next) {
	Module.find({}, function(err, applications) {
		if (err) {
			return next(err);
		}
		else {
			res.json(applications);
		}
	});
};

exports.getSelected = function(req, res, next){
	var moduleIds = req.body.moduleIds;
	if(moduleIds){
		Module.find({
		    '_id': { $in: 
		        moduleIds.split(',')
		    }
		}, function(err, modules){
		     if(err){
		     	return next(err);
		     }else{
		     	res.json(modules);
		     }
		});	
	}else{
		res.send('No selected module')
	}
};

exports.create = function(req, res, next) {
	var module = new Module(req.body);
	module.save(function(err) {
		if (err) {
			return next(err);
		}
		else {
			res.json(module);
		}
	});
};

exports.read = function(req, res) {
	res.json(req.module);
};

exports.getById = function(req, res, next, id) {
	Module.findOne({
		_id: id
	},
	function(err, module) {
		if (err) {
			if(id == 0){
				next();
			}else{
				return next(err);
			}
		}
		else {
			req.module = module;
			next();
		}
	}
	).populate('usergroups');
};

exports.update = function(req, res, next) {
	Module.findByIdAndUpdate(req.module.id, req.body, function(err, module) {
		if (err) {
			return next(err);
		}
		else {
			res.json(module);
		}
	});
};

exports.delete = function(req, res, next) {
	req.module.remove(function(err) {
		if (err) {
			return next(err);
		}
		else {
			res.json(req.module);
		}
	})
};