var Usergroup = require('mongoose').model('Usergroup');

var getErrorMessage = function(err) {
	var message = '';
	for (var errName in err.errors) {
		if (err.errors[errName].message)
			message = err.errors[errName].message;
	}
	return message;
};

exports.create = function(req, res, next) {
	var usergroup = new Usergroup(req.body);
	usergroup.save(function(err) {
		if (err) {
			return next(err);
		}
		else {
			res.json(usergroup);
		}
	});
};

exports.list = function(req, res, next) {
	Usergroup.find({}, function(err, usergroups) {
		if (err) {
			return next(err);
		}
		else {
			res.json(usergroups);
		}
	});
};

exports.getById = function(req, res, next, id) {
	Usergroup.findOne({
		_id: id
	},
	function(err, usergroup) {
		if (err) {
			if(id == 0){
				next();
			}else{
				return next(err);
			}
		}
		else { 
			req.usergroup = usergroup;
			next();
		}
	}
	).populate('qualifiers approvers');
};

exports.getSelected = function(req, res, next){
	var usergroupIds = req.body.usergroupIds;
	if(usergroupIds){
		Usergroup.find({
		    '_id': { $in: 
		        usergroupIds.split(',')
		    }
		}, function(err, usergroups){
		     if(err){
		     	return next(err);
		     }else{
		     	res.json(usergroups);
		     }
		});	
	}else{
		res.send('No selected usergroup');
	}
};
