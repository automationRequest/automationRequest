var mongoose = require('mongoose'),
	Request = mongoose.model('Application'),
 	ObjectId = require('mongoose').Types.ObjectId; 

var http = require('http');
var url = require('url') ;


exports.createView = function(req, res){
	res.render('dashboard/form', {
		title: 'Create Request'
	});
};


exports.list = function(req, res, next) {
	Request.find({}, function(err, applications) {
		if (err) {
			return next(err);
		}
		else {
			res.json(applications);
		}
	}).populate('application usergroup module qualifier qualifiervalue approver');
};