module.exports = function(app) {
	var employee = require('../controllers/employee.server.controller');
	app.route('/employee/getEmployeeByDomainId').post(employee.getEmployeeByDomainId);
	app.route('/employee/getAll').post(employee.getAll);
}