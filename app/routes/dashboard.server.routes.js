module.exports = function(app) {
	var dashboard = require('../controllers/dashboard.server.controller');
	app.route('/dashboard/create').get(dashboard.createView);
	// app.route('/request').get(request.render);
	app.route('/dashboard/list').get(dashboard.list);
	// app.route('/request/searchBy').post(request.searchBy);
	// app.route('/request/batchSend').post(request.batchSend);
	// app.route('/request/approvrView').get(request.approvrView);
	// app.route('/request/approverView').post(request.getRequestBySrNo);
	// app.route('/request/updateStatus/:requestId').post(request.updateSrStatus);
	// app.route('/request/:requestId').get(request.read).put(request.update).delete(request.delete);
	// app.param('requestId', request.getById);
};